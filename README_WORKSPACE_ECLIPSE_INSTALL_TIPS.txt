also install the following plugins into your Juno eclipse installation
- bndtools

bndTools will want to create a cnf project when it first encounters a build, you need to let it.

before you make your first commit to GIT or whatever you use for a SCM, do this:
- Windows > Preferences > Team > Ignored Resources click Add Pattern
- - for each of 3 times add...
- - - target
- - - bin
- - - generated
though this is already done for you, if you use git and generated workspaces (the .gitignore file is also a part of ever project created with this template)

other preferences suggested
- Windows > Preferences > General > Editors > Text Editors select Show Line Numbers checkbox
- Windows > Preferences > Maven > User Interface select the checkbox to show the pom.xml by default